<?php

namespace AppBundle\Entity;

/**
 * History
 */
class History
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $entity;

    /**
     * @var integer
     */
    private $primaryKey;

    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var string
     */
    private $value;

    /**
     * @var \DateTime
     */
    private $changed = 'CURRENT_TIMESTAMP';


    /**
     * Set entity
     *
     * @param string $entity
     *
     * @return History
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set primaryKey
     *
     * @param integer $primaryKey
     *
     * @return History
     */
    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;

        return $this;
    }

    /**
     * Get primaryKey
     *
     * @return integer
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return History
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return History
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     *
     * @return History
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;

        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
