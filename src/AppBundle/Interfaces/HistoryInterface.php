<?php
namespace AppBundle\Interfaces;

interface HistoryInterface
{
    /**
     * @param string $field
     *
     * @return array
     */
    public function getHistory($field);
}
