<?php
namespace AppBundle\Traits;

use AppBundle\Entity\History;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

trait HistoryTrait
{
    /**
     * @var array
     */
    private static $fields = [];
    /**
     * @var string
     */
    private static $entity;
    /**
     * @var array
     */
    private $fieldData = [];
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var History[]
     */
    private $historyList = [];
    /**
     * @var string
     */
    private $dateFormat = 'Y-m-d H:i:s';

    /**
     * @param string $entity
     */
    public static function setEntity($entity)
    {
        self::$entity = $entity;
    }

    /**
     * @param array $fields
     */
    public static function setFields(array $fields)
    {
        self::$fields = $fields;
    }

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $field
     * @param bool   $orderAsc
     *
     * @return array
     */
    public function getHistory($field, $orderAsc = true)
    {
        if (!in_array($field, self::$fields) || !isset(self::$entity)) {
            $this->fieldData[$field] = [];
        }

        if (!isset($this->fieldData[$field])) {
            $historyList = $this->entityManager->getRepository('AppBundle:History')->findBy([
                                                                                                'entity'     => self::$entity,
                                                                                                'primaryKey' => $this->getId(),
                                                                                                'fieldName'  => $field,
                                                                                            ],
                                                                                            [
                                                                                                'changed' => !$orderAsc
                                                                                                    ? 'DESC' : 'ASC',
                                                                                            ])
            ;

            $this->fieldData[$field] = [];
            /** @var History $history */
            foreach ($historyList as $history) {
                $time = $history->getChanged()->format($this->dateFormat);

                $this->fieldData[$field][$time] = $history->getValue();
            }
        }

        return $this->fieldData[$field];
    }

    /**
     * @param \Doctrine\ORM\Event\PreUpdateEventArgs $event
     */
    public function preSaveHistory(PreUpdateEventArgs $event)
    {
        if (!isset(self::$entity)) {
            return;
        }

        $time = new \DateTime('now');

        foreach (self::$fields as $field) {
            if (!$event->hasChangedField($field)) {
                continue;
            }

            $history = new History();
            $history->setEntity(self::$entity)
                    ->setPrimaryKey($this->getId())
                    ->setChanged($time)
                    ->setFieldName($field)
                    ->setValue($event->getOldValue($field))
            ;

            $this->historyList[] = $history;
        }
    }

    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function postSaveHistory(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();

        foreach ($this->historyList as $history) {
            $em->persist($history);
        }

        $em->flush();
    }

    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $event
     */
    public function postLoadHandler(LifecycleEventArgs $event)
    {
        $this->setEntityManager($event->getEntityManager());
    }
}
